﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Dribbble_Reader.ViewXaml
{
    public sealed partial class LoadingScreen : UserControl
    {
        public LoadingScreen()
        {
            this.InitializeComponent();
            UpdateImageSize();
        }

        public void UpdateImageSize()
        {
            var bounds = Window.Current.Bounds;
            double height = bounds.Height;
            double width = bounds.Width;

            ImageCanvas.Width = width;
            ImageCanvas.Height = height;

            ScreenImage.Width = width;
            ScreenImage.Height = height;
        }
    }
}
