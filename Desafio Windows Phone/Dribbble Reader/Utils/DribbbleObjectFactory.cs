﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Dribbble_Reader.Model;

namespace Dribbble_Reader.Utils
{
    public class DribbbleObjectFactory
    {
        /// <summary>
        /// This function deserialize the data JSON object to DribbbleObjectData to be used by application to get information about the shots and users
        /// </summary>
        /// <param name="json">JSON string</param>
        /// <returns></returns>
        public static List<DribbbleObjectData> GetDribbleObjectFromJSON(string json)
        {
            List<DribbbleObjectData> L_dribbleObjList = JsonConvert.DeserializeObject<List<DribbbleObjectData>>(json);
            return L_dribbleObjList;
        }

    }




}
