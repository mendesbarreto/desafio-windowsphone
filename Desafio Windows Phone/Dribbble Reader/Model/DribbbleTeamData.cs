﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dribbble_Reader.Model
{
    public class DribbbleTeamData : DribbbleUserData
    {
        public int members_count { get; set; }
        public string members_url { get; set; }
        public string team_shots_url { get; set; }
    }
}
