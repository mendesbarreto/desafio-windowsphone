﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Graphics.Imaging;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using System.Diagnostics;

using Windows.Phone.UI.Input;

using Dribbble_Reader.Model;
using Dribbble_Reader.Common;

namespace Dribbble_Reader
{
    public sealed partial class ShotImageInfoPage : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        private DribbbleObjectData m_dribbleData;
        private Image m_perfilPhoto;
        private Image m_image;

        public ShotImageInfoPage()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);

            m_dribbleData = (DribbbleObjectData)e.Parameter;
            UpdateScreen();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void UpdateScreen()
        {
            string L_htmlText = "<font size=\"2\"color=\"#FFFFFF\">{0}</font>";
            
            m_perfilPhoto = new Image();
            m_perfilPhoto.Source = new BitmapImage(new Uri(m_dribbleData.user.avatar_url));

            m_image = new Image();
            m_image.Source = new BitmapImage(new Uri(GetAvailableUrlImage(m_dribbleData.images)));

            authorImage.ImageSource = m_perfilPhoto.Source;
            ImageHolder.Source = m_image.Source;
            authorNameTextBox.Text = m_dribbleData.user.name;


            L_htmlText = String.Format(L_htmlText, m_dribbleData.description);
            description.NavigateToString(L_htmlText);

            LoadingScreenController.Hide();
        }

        private string GetAvailableUrlImage(DribbbleImageData imageData)
        {
            string L_urlImage = "";

            if (m_dribbleData.images.hidpi != null && m_dribbleData.images.hidpi != "")
                L_urlImage = m_dribbleData.images.hidpi;
            else if (m_dribbleData.images.normal != null && m_dribbleData.images.normal != "")
                L_urlImage = m_dribbleData.images.normal;
            else if (m_dribbleData.images.teaser != null && m_dribbleData.images.teaser != "")
                L_urlImage = m_dribbleData.images.teaser;
            else
            {
                //TRHOW A ERROR IMAGE URL IS EMPTY
            }

            return L_urlImage;
        }
// 
//         private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
//         {
//             Frame frame = Window.Current.Content as Frame;
// 
//             if (frame == null)
//             {
//                 return;
//             }
// 
//             if (frame.CanGoBack)
//             {
//                 LoadingScreenController.Show();
//                 Frame.Navigate(typeof(ShotsPage));
//                 e.Handled = true;
//             }
//         }
    }
}
