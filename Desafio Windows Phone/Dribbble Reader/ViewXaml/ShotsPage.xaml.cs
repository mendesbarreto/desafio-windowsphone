﻿using Dribbble_Reader.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Diagnostics;
using Windows.Graphics.Imaging;
using Windows.UI.Xaml.Media.Imaging;
using System.Windows;
using Dribbble_Reader.Utils;
using Dribbble_Reader.Model;

using Windows.Phone.UI.Input;

using Windows.ApplicationModel.Activation;
using Windows.UI.Core;
using Dribbble_Reader.ViewXaml;
using Dribbble_Reader.Common;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Dribbble_Reader
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ShotsPage : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        
        /// <summary>
        /// Contains the Current page number
        /// </summary>
        private int m_pageNumber;
        
        /// <summary>
        /// Contains the last item index in the m_shotList
        /// </summary>
        private int m_itemIndex;

        /// <summary>
        /// Contains the list of information of all downloaded items
        /// </summary>
        List<DribbbleObjectData> m_shotsList;

        /// <summary>
        /// Contains the current category page
        /// </summary>
        private string m_pageCategory;

        /// <summary>
        /// Contains the last category page
        /// </summary>
        private string m_lastPageCategory;

        /// <summary>
        /// Checks whether the application is performing the request to the Dribbble or no
        /// </summary>
        private bool m_isRequestShots = false;

        /// <summary>
        /// Checks if the application page was initialized
        /// </summary>
        private bool m_isPageInitialized = false;

        public ShotsPage()
        {
            m_pageNumber = 1;
            m_itemIndex = 0;
            m_pageCategory = DribbbleUrlHelper.LIST_ALL;

            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;


            Debug.WriteLine("Constructor");
        }
        
        
        /// <summary>
        /// Initialize the Page 
        /// </summary>
        private void Init()
        {
            try
            { 
                if (!m_isPageInitialized || m_pageCategory != m_lastPageCategory)
                {
                    ImageListBox.Items.Clear();
                    m_itemIndex = 0;
                    m_shotsList = new List<DribbbleObjectData>();

                    ExecuteRequest();
                    m_isPageInitialized = true;
                }
                else
                {
                    LoadingScreenController.Hide();
                }
            }catch( Exception ex )
            {
                Debug.WriteLine("[ERROR] Problem to initialize the page" + ex.Message + "\n" + ex.StackTrace);

                if( this.navigationHelper.CanGoBack() )
                {
                    this.navigationHelper.GoBack();
                }
            }
            //HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        /// <summary>
        /// Builds and makes the request to the Dribble 
        /// </summary>
        private void ExecuteRequest()
        {
            if (!m_isRequestShots)
            { 
                m_isRequestShots = true;
                Dribbble_Reader.Request.DribbbleRequest request = new Dribbble_Reader.Request.GetShotsByListNameRequest(m_pageCategory, m_pageNumber);
                request.OnRequestReceived.AddOnce(OnDribbleRequestReceivedHandler);
                request.Execute();
            }
            else
            {
               
            }
        }

        
        /// <summary>
        /// Invoked when the application receive the request from server 
        /// </summary>
        /// <param name="json">JSON data string</param>
        private void OnDribbleRequestReceivedHandler( string json )
        {
            Debug.WriteLine(json);
            m_shotsList.InsertRange( m_itemIndex , DribbbleObjectFactory.GetDribbleObjectFromJSON(json) );
            UpdateShotsListAndScreen();
            LoadingScreenController.Hide();
            m_isRequestShots = false;
        }


        /// <summary>
        /// Get the next page of shots
        /// </summary>
        public void NextShotsPage()
        {
            if (!m_isRequestShots)
            {
                m_pageNumber++;
                ExecuteRequest();
            }
            else
            {
                Debug.WriteLine("We can't do that DuDE!");
            }
        }


        /// <summary>
        /// When new shots are received this function updates the shots lists on the screen
        /// </summary>
        public void UpdateShotsListAndScreen()
        {
            Image foto;
            DribbbleObjectData obj;
            ImageHolderInterface imageHolder;

            try
            {
                for (; m_itemIndex < m_shotsList.Count - 1; ++m_itemIndex)
                {
                    imageHolder = new ImageHolderInterface();

                    obj = m_shotsList[m_itemIndex];
                    foto = new Image();
                    foto.Source = new BitmapImage(new Uri(obj.images.normal));

                    imageHolder.SetPhoto(foto);
                    imageHolder.SetLikesCount(obj.likes_count);
                    imageHolder.SetCommentsCount(obj.comments_count);
                    imageHolder.SetBucketCount(obj.buckets_count);
                    imageHolder.SetViewsCount(obj.views_count);
                    imageHolder.SetDribbleData(obj);

                    ImageListBox.Items.Add(imageHolder);
                }
            }catch(Exception ex )
            {
                Debug.WriteLine("[ERROR: Fail to update the list: ]" + ex.Message + "\n" + ex.StackTrace);

                if( this.navigationHelper.CanGoBack() )
                {
                    this.navigationHelper.GoBack();
                }
            }
        }


       /// <summary>
       /// This function is invoked when the scroll in the page screen changes
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        private void OnScrollViewerViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            var verticalOffset = ImageScrollViwer.VerticalOffset;
            //Debug.WriteLine("Is it the end?");

            if (verticalOffset == ImageScrollViwer.ScrollableHeight && !m_isRequestShots)
            {
                LoadingScreenController.Show();
                NextShotsPage();
                //Debug.WriteLine("YES SIR!");
            }
            //else
                //Debug.WriteLine("NO DUDE!");
            // ... 
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            Debug.WriteLine("State Load");
        }


        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            Debug.WriteLine("State Save");
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
            m_pageCategory = e.Parameter as string;
            Init();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            m_lastPageCategory = m_pageCategory;
            this.navigationHelper.OnNavigatedFrom(e);
            //HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }

        #endregion
        /// <summary>
        /// This function is invoked when the user selects some item inside the ListBox on the screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImageListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            ListBox L_listBox = (ListBox) sender;
            ImageHolderInterface L_holder;
            try
            {
                if (L_listBox != null)
                {
                    L_holder = L_listBox.SelectedItem as ImageHolderInterface;

                    if (L_holder != null)
                    {
                        Debug.WriteLine(L_holder);
                        LoadingScreenController.Show();
                        Frame.Navigate(typeof(ShotImageInfoPage), L_holder.GetDribbleData());
                    }
                    else
                    {
                        //TODO: Throw the SelectedItem not is a ImageHolderInterface
                    }
                }
                else
                {
                    //TODO: Throw the sender not is a ListBox
                }
                Debug.WriteLine("Hey Dude");
            }catch( Exception ex )
            {
                Debug.WriteLine("[ERROR: Fail to update the list: ]" + ex.Message + "\n" + ex.StackTrace);

                if (this.navigationHelper.CanGoBack())
                {
                    this.navigationHelper.GoBack();
                }
            }
        }

    }
}
