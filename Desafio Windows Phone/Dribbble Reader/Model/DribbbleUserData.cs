﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dribbble_Reader.Model
{
    public class DribbbleUserData
    {
        public int id { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public string html_url { get; set; }
        public string avatar_url { get; set; }
        public string bio { get; set; }
        public object location { get; set; }
        public int buckets_count { get; set; }
        public int comments_received_count { get; set; }
        public int followers_count { get; set; }
        public int followings_count { get; set; }
        public int likes_count { get; set; }
        public int likes_received_count { get; set; }
        public int projects_count { get; set; }
        public int rebounds_received_count { get; set; }
        public int shots_count { get; set; }
        public int teams_count { get; set; }
        public bool can_upload_shot { get; set; }
        public string type { get; set; }
        public bool pro { get; set; }
        public string buckets_url { get; set; }
        public string followers_url { get; set; }
        public string following_url { get; set; }
        public string likes_url { get; set; }
        public string projects_url { get; set; }
        public string shots_url { get; set; }
        public string teams_url { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }

    }
}
