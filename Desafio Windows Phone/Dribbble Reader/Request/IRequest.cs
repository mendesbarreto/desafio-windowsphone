﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dribbble_Reader.Request
{
    interface IRequest
    {
        string GetUrl();
        void SetUrl(string url);
    }
}
