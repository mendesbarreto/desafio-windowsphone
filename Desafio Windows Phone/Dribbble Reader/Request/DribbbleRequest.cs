﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.IO;
using System.Net;
using Dribbble_Reader.Utils;
using System.Diagnostics;
using Signals;

namespace Dribbble_Reader.Request
{

    /// <summary>
    /// This class was created to realize the request process for Dribbble API. 
    /// It uses my personal library SIGNALS which is based on Boost::Signals for C ++.
    /// </summary>
    /// <see cref="https://github.com/mendesbarreto/SignalsCSharpApi"/>
    public class DribbbleRequest : IRequest
    {
        /// <summary>
        /// This event is dispatched when the URL changes
        /// </summary>
        public Signal<string> OnUrlChange = new Signal<string>();

        /// <summary>
        /// This event is dispatched when the request is received
        /// </summary>
        public Signal<string> OnRequestReceived = new Signal<string>();

        /// <summary>
        /// This event is dispatched when the request is sent 
        /// </summary>
        public Signal<bool> OnRequestSent = new Signal<bool>();

        /// <summary>
        /// URL to be requested
        /// </summary>
        protected string m_url = "";

        /// <summary>
        /// The Request Item
        /// </summary>
        protected HttpWebRequest m_request;

        /// <summary>
        /// The Request Response
        /// </summary>
        protected HttpWebResponse m_response;

        /// <summary>
        /// The response of request in string
        /// </summary>
        protected string m_content = "";
        public string Content
        {
            get
            {
                return m_content;
            }

            set
            {
                m_content = value;
            }
        }


        /// <summary>
        /// Execute the request to server
        /// </summary>
        public void Execute()
        {
            Send();
        }

        protected virtual async void Send()
        {
            OnRequestReceived.Dispatch(Content);
        }

        /// <summary>
        /// This method will build the http request to get the information by the application needs
        /// </summary>
        /// <param name="url">Insert here the Dribbble API URL</param>
        /// <returns></returns>
        public async Task<string> GetDataFrom(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = Dribbble_Reader.Utils.HttpMethod.GET;
            HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                return sr.ReadToEnd();
            }
        }

        public string GetUrl()
        {
            return m_url;
        }

        public void SetUrl( string url )
        {
            if( m_url != url )
            {
                m_url = url;
                OnUrlChange.Dispatch(url);
            }
        }

        public void Clear()
        {
            OnRequestReceived.RemoveAll();
            OnRequestSent.RemoveAll();
            OnUrlChange.RemoveAll();
            SetUrl("");
        }


        //         TESTE REQUEST
        //         public async Task<string> GetWebsync(string url)
        //         {
        //             HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
        //             request.Method = Dribbble_Reader.Utils.HttpMethod.GET;
        //             Task<WebResponse> L_task = request.GetResponseAsync();
        //             HttpWebResponse response = (HttpWebResponse) await L_task;
        //             using (var sr = new StreamReader(response.GetResponseStream()))
        //             {
        //                 return sr.ReadToEnd();
        //             }
        //         }
        //         
        //         public async Task<string> GetWebPageAsync(string url)
        //         {
        //             HttpClient http = new HttpClient();
        //             HttpResponseMessage response = await http.GetAsync(url);
        //             return await response.Content.ReadAsStringAsync();
        //         }

    }


}
