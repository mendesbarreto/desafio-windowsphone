﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Dribbble_Reader.Utils;


namespace Dribbble_Reader.Request
{
    public class GetShotsByListNameRequest : DribbbleRequest
    {
        private int m_pageNumber;
        private int m_qtdItensPerPage;
        private string m_category;
        private string m_sortBy;

        public GetShotsByListNameRequest(
            string categoryPage,
            int pageNumber,
            string sortBy = DribbbleUrlHelper.SORT_SHOTS_BY_MOST_POPULAR,
            int qtdItensPerPage = DribbbleUrlHelper.QTD_DEFAULT_ITENS_PER_PAGE)
        {
            m_category = categoryPage;
            m_pageNumber = pageNumber;
            m_qtdItensPerPage = qtdItensPerPage;
            m_sortBy = sortBy;
        }

        protected override async void Send()
        {
            try
            {
                Debug.WriteLine("My URL: " + DribbbleUrlHelper.GetShotsUrlByListName( m_category, m_pageNumber, m_qtdItensPerPage, m_sortBy ) );
                Task<string> getWebPageTask = GetDataFrom(DribbbleUrlHelper.GetShotsUrlByListName(m_category, m_pageNumber, m_qtdItensPerPage, m_sortBy ));
                Debug.WriteLine("In startButton_Click before await");
                string webText = await getWebPageTask;
                Debug.WriteLine("Characters received: " + webText.Length.ToString());
                Content = webText;
            }catch(Exception ex )
            {
                Debug.WriteLine("[ERROR] Problem to make the request " + ex.Message + "\n" + ex.StackTrace);
            }

            base.Send();
        }
    }
}
