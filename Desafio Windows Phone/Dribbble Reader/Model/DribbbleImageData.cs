﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dribbble_Reader.Model
{
    public class DribbbleImageData
    {
        public string hidpi { get; set; }
        public string normal { get; set; }
        public string teaser { get; set; }
    }
}
