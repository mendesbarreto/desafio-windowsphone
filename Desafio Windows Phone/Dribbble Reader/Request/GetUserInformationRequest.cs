﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Dribbble_Reader.Common;
using Dribbble_Reader.Utils;

namespace Dribbble_Reader.Request
{
    public class GetUserInformationRequest : DribbbleRequest
    {
        protected override async void Send()
        {
            Debug.WriteLine("My URL: " + DribbbleUrlHelper.GetUserInfoUrl());
            Task<string> getWebPageTask = GetDataFrom(DribbbleUrlHelper.GetUserInfoUrl());
            
            Debug.WriteLine("In startButton_Click before await");

            string webText = await getWebPageTask;
            
            Debug.WriteLine("Characters received: " + webText.Length.ToString());

            Content = webText;

            base.Send();
        }
        
    }
}
