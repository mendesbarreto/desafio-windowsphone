﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dribbble_Reader.Common;

namespace Dribbble_Reader.Utils
{
    /// <summary>
    /// The DribbleUrlHelper was built to help the programmer in the moment of creating the necessary URLs to make requests to the DribbbleAPI
    /// </summary>
    public sealed class DribbbleUrlHelper
    {
        /// <summary>
        /// The Main API url
        /// </summary>
        public const string API_URL = "https://api.dribbble.com/v1/{0}?";

        //////////////////////////////////////////////////////////////////////////////////////////////
        /////////////Here the constants needed to build the API URL to request information////////////
        //////////////////////////////////////////////////////////////////////////////////////////////

        public const string PARAM_SHOTS = "shots";
        public const string PARAM_USER = "user";

        public const string PARAM_ACCESS_TOKEN = "access_token";
        public const string PARAM_PAGE = "page";
        public const string PARAM_ITENS_PER_PAGE = "per_page";
        
        public const string PARAM_LIST = "list";
        public const string LIST_ALL = "";
        public const string LIST_ANIMATED = "animated";
        public const string LIST_ATTACHEMENTS = "attachments";
        public const string LIST_DEBUTS = "debuts";
        public const string LIST_PLAYOFFS = "playoffs";
        public const string LIST_REBOUNDS = "rebounds";
        public const string LIST_TEAMS = "teams";

        public const string PARAM_SORT = "sort=";
        public const string SORT_SHOTS_BY_MOST_VIEWED = "views";
        public const string SORT_SHOTS_BY_MOST_POPULAR = "";
        public const string SORT_SHOTS_BY_MOST_COMMENTED = "comments";

        ////////////////////////////////////////// END /////////////////////////////////////////////

        /// <summary>
        /// Specifies the current amount of items that will contain each page
        /// </summary>
        public const int QTD_DEFAULT_ITENS_PER_PAGE = 20;


        /// <summary>
        /// This function creates a unique URL to query a specific shot information using your ID.
        /// </summary>
        /// <param name="id"> Shot identifier.
        /// <returns>string</returns>
        /// This parameter is typically used to configure the page.</param>
        static public string GetShotUrlByShotID( string id )
        {
            string L_url = API_URL;
            AddParam(ref L_url, PARAM_SHOTS + "/" + id);
            AddQueryIten(ref L_url, PARAM_ACCESS_TOKEN, ApplicationInfo.ACCESS_TOKEN);
            return L_url;
        }

        /// <summary>
        /// This function creates a unique URL to query the user information.
        /// </summary>
        /// <returns>string</returns>
        static public string GetUserInfoUrl()
        {
            string L_url = API_URL;

            AddParam(ref L_url, PARAM_USER);
            AddQueryIten(ref L_url, PARAM_ACCESS_TOKEN, ApplicationInfo.ACCESS_TOKEN);

            return L_url;
        }

        /// <summary>
        /// This function creates a unique URL to query a shots list information using a list name to segment the informations by categories.
        /// </summary>
        /// <param name="listName"> Identifier to each category that will be requested.
        static public string GetShotsUrlByListName(string listName)
        {
            return GetShotsUrlByListName( listName, 0, QTD_DEFAULT_ITENS_PER_PAGE, SORT_SHOTS_BY_MOST_POPULAR );
        }

        /// <summary>
        /// This function creates a unique URL to query a shots list information using a list name to segment the informations by categories.
        /// </summary>
        /// <param name="listName"> Identifier to each category that will be requested.
        /// <param name="pageNumber"> Page number that will be requested
        /// <param name="qtdItensPerPage"> Insert here how many items you want request in the list.
        /// <param name="sortBy"> Insert here which kind of sorting your list will be organized.
        /// <returns>string</returns>
        static public string GetShotsUrlByListName(
            string listName = LIST_ALL, 
            int pageNumber = 0, 
            int qtdItensPerPage = QTD_DEFAULT_ITENS_PER_PAGE,
            string sortBy = SORT_SHOTS_BY_MOST_POPULAR)
        {
            string L_url = API_URL;

            AddParam(    ref L_url, PARAM_SHOTS);
            AddQueryIten(ref L_url, PARAM_LIST, listName);
            AddQueryIten(ref L_url, PARAM_SORT, sortBy);
            AddQueryIten(ref L_url, PARAM_PAGE, pageNumber.ToString());
            AddQueryIten(ref L_url, PARAM_ITENS_PER_PAGE, qtdItensPerPage.ToString());
            AddQueryIten(ref L_url, PARAM_ACCESS_TOKEN, ApplicationInfo.ACCESS_TOKEN);

            return L_url;
        }

        /// <summary>
        /// This function creates a param for Dribble URL
        /// </summary>
        /// <param name="url"> main url.
        /// <param name="param"> Insert the param name.
        /// <returns>string</returns>
        static public string AddParam(ref string url, string param)
        {
            return ( url = String.Format(url, param) );
        }

        /// <summary>
        /// This function creates a query param for Dribble URL
        /// </summary>
        /// <param name="url"> The URL you want to add the query param.
        /// <param name="name"> Insert the param name.
        /// <param name="value"> Insert the param value.
        /// <returns>string</returns>
        static public string AddQueryIten(ref string url, string name, string value)
        {
            if (value != "")
                return ( url = url + name + "=" + value + "&" );

            return url;
        }


    }

    public static class HttpMethod
    {
        public static string GET = "GET";
        public static string POST = "POST";
    }
}
