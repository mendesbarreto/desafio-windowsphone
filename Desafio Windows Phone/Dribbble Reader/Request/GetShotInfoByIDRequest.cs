﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Dribbble_Reader.Request;
using Dribbble_Reader.Utils;

namespace Dribbble_Reader.Request
{
    public class GetShotInfoByIDRequest : DribbbleRequest
    {
        private string m_ID = "";

        public GetShotInfoByIDRequest( string id)
        {
            m_ID = id;
        }

        protected override async void Send()
        {
            Debug.WriteLine("My URL: " + DribbbleUrlHelper.GetShotUrlByShotID(m_ID) );
            Task<string> L_getWebPageTask = GetDataFrom(DribbbleUrlHelper.GetShotUrlByShotID(m_ID));

            Debug.WriteLine("In startButton_Click before await");

            string webText = await L_getWebPageTask;

            Debug.WriteLine("Characters received: " + webText.Length.ToString());

            Content = webText;

            base.Send();
        }
    }
}
