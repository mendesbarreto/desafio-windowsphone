﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;

namespace Dribbble_Reader.Common
{
    /// <summary>
    /// This class was created to control when the application will display the loading page while some process are running.
    /// </summary>
    public class LoadingScreenController
    {
        private static Popup m_popUp = new Popup();
        private static UserControl m_loadingScreen;

        /// <summary>
        /// Sets which screen will be used
        /// </summary>
        public static void SetPopUp( UserControl screen )
        {
            m_loadingScreen = screen;
        }

        /// <summary>
        /// Show the screen
        /// </summary>
        public static void Show()
        {
             m_popUp.Child = m_loadingScreen;
             m_popUp.IsOpen =true;
        }

        /// <summary>
        /// Hide the screen
        /// </summary>
        public static void Hide()
        {
            m_popUp.IsOpen =false;
        }
    }
}
