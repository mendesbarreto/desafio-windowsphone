﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using Dribbble_Reader.Model;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Dribbble_Reader
{
    public sealed partial class ImageHolderInterface : UserControl
    {
        private DribbbleObjectData m_dribbleData;


        public ImageHolderInterface()
        {
            this.InitializeComponent();
        }

        public void SetPhoto( Image photo )
        {
            imageHolder.Source = photo.Source;
        }

        public void SetLikesCount( int value )
        {
            likes.Text = value.ToString();
        }

        public void SetCommentsCount( int value)
        {
            comments.Text = value.ToString();
        }

        public void SetBucketCount( int value)
        {
            bucket.Text = value.ToString();
        }

        public void SetViewsCount(int value)
        {
            views.Text = value.ToString();
        }

        public DribbbleObjectData GetDribbleData()
        {
            return m_dribbleData;
        }

        public void SetDribbleData(DribbbleObjectData data)
        {
            m_dribbleData = data;
        }
    }
}
