﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dribbble_Reader.Model
{
    public class DribbbleObjectData
    {
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public DribbbleImageData images { get; set; }
        public int views_count { get; set; }
        public int likes_count { get; set; }
        public int comments_count { get; set; }
        public int attachments_count { get; set; }
        public int rebounds_count { get; set; }
        public int buckets_count { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public string html_url { get; set; }
        public string attachments_url { get; set; }
        public string buckets_url { get; set; }
        public string comments_url { get; set; }
        public string likes_url { get; set; }
        public string projects_url { get; set; }
        public string rebounds_url { get; set; }
        public List<string> tags { get; set; }
        public DribbbleUserData user { get; set; }
        public DribbbleTeamData team { get; set; }
    }
}
